from django.apps import AppConfig


class FtmapConfig(AppConfig):
    name = 'ftmap'
